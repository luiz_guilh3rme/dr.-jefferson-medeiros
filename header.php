<!DOCTYPE html>
<html lang="pt-BR">
<head>
<title><?php wp_title(); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=1.0">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5HQFFB3');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HQFFB3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="main-container">
<?php global $mydata; ?>
<div class="menu-button">
	<div class="menu-button-center">
	</div>
</div>
<header>
	<div class="centered-container cf">
		<div class="header-logo">
			<a href="<?php echo  esc_url(home_url()); ?>">
				<img src="<?php echo $mydata->header_logo ?>" alt="RoraimaNET" title="RoraimaNET">
			</a>
		</div>
		<div class="top-phones-box">
			<p><i class="fa fa-fw fa-lg fa-phone"></i> <?php echo $mydata->phones; ?></p>
			<p><i class="fa fa-fw fa-lg fa-whatsapp"></i> <a style="color:white;" href="wpp"><?php echo $mydata->whatsapp; ?></a></p>
		</div>
	</div>
</header>

<!-- INICIO Menu e Banner -->
<section class="relative menu">
	<nav class="main-menu" <?php if(is_front_page()){ echo 'style="position:absolute;"'; } ?>>
		<div class="centered-container">
		<?php
			$args = array(
				'theme_location' => 'Menu Princicpal',
				'menu' => 'Menu Principal',
				'container_class' => 'menu-nav-container',
				'container_id' => '',
				'menu_class' => 'menu',
				'walker' => new WPSE_78121_Sublevel_Walker
			);

			wp_nav_menu( $args ); ?>

			
		</div>
	</nav>
</section>

