<?php get_header(); ?>


<section class="page-content">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span>Pesquisa</span>
				</h1>
			</div>
		</div>
		
		<div class="entry-content cf">
			<div style="margin-bottom: 50px">
		<?php 
			if(have_posts()):
				while(have_posts()) : the_post(); 
		?>
		

				<a class="search-result" href="<?php the_permalink(); ?>">
					 <h3><?php the_title(); ?></h3>
					 <p><?php echo get_the_excerpt(); ?></p>
				</a>

		<?php   
			endwhile; 
		?>
			</div>
		
		</div>
		
		<?php 
			else: 
		?>
			<h1>Nenhum resultado encontrado para: <span style="color: #900"><?php echo get_search_query() ?></span></h1>
		<?php 
			endif;
		 ?>

		 <div class="pagination">
			<?php
				global $wp_query;

				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages,
					'prev_text' => __('«'),
					'next_text' => __('»')
				) );
			?>
		</div>

	</div>

</section>

<?php get_template_part('part-noticias'); ?>

<?php get_template_part('part-faca-parte'); ?>

<?php get_footer(); ?>
