<div class="procedures-list-wrapper <?php if(is_front_page()){ echo 'centered'; } ?>">	
	<ul class="procedures-list">
		<li>Procedimentos</li>
	<?php 
		$proc = null;

		if(is_front_page()){
			$proc = get_pages(array('parent'=>252, 'sort_column'=>'menu_order', 'number'=>7));
		}
		else{
			$proc = get_pages(array('parent'=>252,'sort_column'=>'menu_order'));
		}
		
		foreach($proc as $p) :
	?>
		<li>
			<a href="<?php echo get_page_link($p->ID); ?>"><?php echo $p->post_title ?></a>
		</li>
	<?php 
		endforeach;
		if(is_front_page()):
	 ?>
	 	<li class="text-center">
	 		<a href="<?php echo get_page_link(252); ?>">VER TODOS</a>
	 	</li>
	 <?php 
	 	endif;
	  ?>
	</ul>
	<?php 
		if(!is_front_page()):
	 ?>
	 <span class="fa fa-plus showProc" title="Clique para ver mais procedimentos"></span>
	 <?php 
	 	endif;
	  ?>
</div>

