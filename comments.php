<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<h2>Comentários!</h2>

	<?php if ( have_comments() ) : ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 180,
				) );
			?>
		</ol><!-- .comment-list -->



	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfifteen' ); ?></p>
	<?php endif; ?>

	<?php 
		$fields = array(
			'author' =>
		    '<p class="comment-form-author"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
		    '" size="30"' . $aria_req . ' placeholder="Nome" /></p>',

		  'email' =>
		    '<p class="comment-form-email"><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
		    '" size="30"' . $aria_req . '  placeholder="Email"  required /></p>',

		   'url'=> ''
		);
		comment_form(
			array(
				'fields' => apply_filters( 'comment_form_default_fields', $fields ),
				'comment_notes_before' =>'<p>Deixe seu comentário.</p>',
				'class_submit' => 'btn btn-red',
				'label_submit' => 'enviar',
				 'title_reply'       => '',
				 'comment_field' =>  '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Digite seu comentário..." >' .
			    '</textarea></p>',
			)
		);
	?>

</div><!-- .comments-area -->
