<?php get_header(); ?>


<section class="page-content page-<?php echo $post->post_name ?>">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span><?php the_title(); ?></span>
				</h1>
			</div>
			<div class="col-xs-12">
		
				<?php  
					$children = get_pages(array('parent'=>$post->ID));
					if(!empty($children)):
						foreach($children as $post): setup_postdata( $post );
				?>
					
					<a class="search-result" href="<?php the_permalink(); ?>">
						 <h3><?php the_title(); ?></h3>
						 <p><?php echo get_the_excerpt(); ?></p>
					</a>	

				<?php
						endforeach;
					else:
						while(have_posts()) : the_post(); 

							the_content();

						endwhile; 
					endif;
				?>
				
				<!-- <div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">A</li>
							<li>Abscesso Cervical</li>
							<li>Adenoma Tireoidiano</li>
							<li>Adenoma Parotídeo</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">B</li>
							<li>Bócio</li>
							<li>Bócio Endêmico</li>
							<li>Bócio Lingual</li>
							<li>Bócio Nodular</li>
							<li>Bócio Subesternal</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">C</li>
							<li>Câncer da língua</li>
							<li>Câncer de laringe</li>
							<li>Carcinoma Papilar</li>
							<li>Carcinoma Papilar, Variante Folicular</li>
							<li>Cistos Branquiais</li>
							<li>Cisto de Tireoglosso</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">D</li>
							<li>Doenças da Glândula Tireóide</li>
							<li>Doenças da Língua</li>
							<li>Doenças das Glândulas Salivares</li>
							<li>Doenças das Paratireóides</li>
							<li>Doenças Faríngeas</li>
							<li>Doenças Maxilares</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">E</li>
							<li>Estenose Traqueal</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">F</li>
							<li>Ectropio</li>
							<li>Fístula das Glândulas Salivares</li>
							<li>Fraturas Maxilomandibulares</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">H</li>
							<li>Hemangioma</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">L</li>
							<li>Lesões do Pescoço</li>
							<li>Lipoma</li>
						</ul>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">N</li>
							<li>Neoplasias Bucais</li>
							<li>Neoplasias da Glândula Tireóide</li>
							<li>Neoplasias de Cabeça E Pescoço</li>
							<li>Neoplasias Faciais</li>
							<li>Neoplasias Laríngeas</li>
							<li>Nódulo da Glândula Tireóide</li>
						</ul>
					</div>


					<div class="col-xs-12 col-sm-4 col-md-4">
						<ul class="disease-list">
							<li class="list-header">S</li>
							<li>Sialolitiase</li>
						</ul>
					</div> -->

				</div>

		
			</div>
		</div>
		<?php get_template_part('part-share'); ?>
	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>

