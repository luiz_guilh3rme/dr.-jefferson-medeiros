<?php

	// @ini_set( 'upload_max_size' , '64M' );
	// @ini_set( 'post_max_size', '64M');
	// @ini_set( 'max_execution_time', '300' );

	include_once 'functions/custom_functions.php';

	add_theme_support('post-thumbnails');
	// function ikreativ_async_scripts($url){
	//     if ( strpos( $url, '#asyncload') === false )
	//         return $url;
	//     else if ( is_admin() )
	//         return str_replace( '#asyncload', '', $url );
	//     else
	// 	return str_replace( '#asyncload', '', $url )."' async='async"; 
	// }
	// add_filter( 'clean_url', 'ikreativ_async_scripts', 11, 1 );

	function soma_scripts(){
		if(!is_admin()){
			wp_deregister_script('jquery');
		   	wp_register_script('jquery', get_stylesheet_directory_uri() . '/js/jquery.js', false, '2.1.4',TRUE);
		   	wp_enqueue_script('jquery');
		}

		wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('flexboxgrid.min', get_stylesheet_directory_uri(). '/css/flexboxgrid.min.css');

		wp_enqueue_style('slickcss', get_stylesheet_directory_uri(). '/css/slick.css');
		wp_enqueue_style('style', get_stylesheet_directory_uri(). '/style.css');


		// wp_enqueue_script('instafeed', get_stylesheet_directory_uri() . '/js/instafeed.js',null,'',TRUE );
		// wp_enqueue_script('stellar', get_stylesheet_directory_uri() . '/js/stellar.min.js',null,'',TRUE );
		wp_enqueue_script('scrollreveal', get_stylesheet_directory_uri() . '/js/scrollreveal.min.js',null,'',TRUE );
		wp_enqueue_script('slickjs', get_stylesheet_directory_uri() . '/js/slick.min.js',null,'',TRUE );
		wp_enqueue_script('main', get_stylesheet_directory_uri() . '/js/main.js',null,'',TRUE );
		wp_enqueue_script('SmoothScroll', get_stylesheet_directory_uri() . '/js/SmoothScroll.min.js',null, '',TRUE);

		wp_localize_script( 'main', 'projett', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		));
	}
	add_action('wp_enqueue_scripts','soma_scripts');


	// function add_async_attribute($tag, $handle) {
	//    // add script handles to the array below
	//    $scripts_to_async = array('scrollreveal', 'slickjs','SmoothScroll','main');
	   
	//    foreach($scripts_to_async as $async_script) {
	//       if ($async_script !== $handle) {
	//          return str_replace(' src', ' async="async" src', $tag);
	//       }
	//    }
	//    return $tag;
	// }
	// add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


	// function wpdocs_theme_add_editor_styles() {
	//     add_editor_style('/css/bootstrap.css');
	// }
	// add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

	function cutom_login_logo() {
		echo "<style type=\"text/css\">
		body.login div#login h1 a {
		background-image: url(".get_bloginfo('template_directory')."/img/login-logo.png);
		-webkit-background-size: auto;
		background-size: auto;
		margin: 0 0 25px;
		width: 320px;
		}
		</style>";
	}
	add_action( 'login_enqueue_scripts', 'cutom_login_logo' );

	// set_post_thumbnail_size( 150, 150, true );
	// add_image_size( 'category-thumb', 655, 250);
	add_image_size( 'news-thumb', 320, 210,true);
	add_image_size( 'gallery-thumb', 250, 165,true);

	add_filter( 'image_size_names_choose', 'my_custom_sizes' );
	function my_custom_sizes( $sizes ) {
	    return array_merge( $sizes, array(
	        'news-thumb' => __( 'Notícias')
	    ) );
	}

	if ( function_exists( 'register_nav_menu' ) ) {
		register_nav_menu( 'menu', 'Menu Principal');
		// register_nav_menu( 'top-menu', 'Menu do Topo');
		register_nav_menu( 'footer-menu', 'Menu do Rodapé');
	}

	function remove_wp_logo( $wp_admin_bar ) {
		global $wp_admin_bar;
		$wp_admin_bar->remove_node( 'wp-logo' );
	}
	add_action( 'wp_before_admin_bar_render', 'remove_wp_logo', 999 );

	add_filter('admin_footer_text', 'bl_admin_footer');
	function bl_admin_footer() {
	  echo '&copy;2017 Dr. Jefferson Medeiros - Desenvolvido por <a href="http://somavirtual.com.br" target="_blank">Soma Virtual</a>';
	}


	function wps_change_role_name() {
	global $wp_roles;
	if ( ! isset( $wp_roles ) )
		$wp_roles = new WP_Roles();
		$wp_roles->roles['subscriber']['name'] = 'Aluno';
		$wp_roles->role_names['subscriber'] = 'Aluno';
	}
	add_action('init', 'wps_change_role_name');



	function wpdocs_theme_add_editor_styles() {
	    add_editor_style( 'css/flexboxgrid.min.css');
	}
	add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

	function custom_excerpt_length( $length ) {
		return 15;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

?>
