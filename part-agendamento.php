<section class="scheduling-section">
	<div class="centered-container row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-6"  col-md-6 col-md-offset-6">
			<h2 class="reveal-up">Pergunte ao<br><span>Dr. Jefferson Medeiros</span></h2>
			<div class="scheduling-form reveal-up">
				<?php echo do_shortcode('[contact-form-7 id="6" title="Pré-Agendamento"]'); ?>
			</div>
		</div>		
	</div>
</section>
<section class="testimonials-section reveal-up">
	<h2 class="reveal-up">Veja depoimentos</h2>
	<div class="testimonials-carousel-wrapper">
		<div class="centered-container">
			<div class="testimonials-carousel">
				<?php 
					$test = new WP_Query(array('post_type'=>'depoimento','posts_per_page'=>-1));
					while($test->have_posts()) : $test->the_post();
				 ?>

				<div>
					<div class="testimonial">
						<blockquote>
							<?php echo get_field('depoimento') ?>
						</blockquote>
						<p>- <?php the_title(); ?></p>
					</div>	
				</div>
				
				<?php 
					endwhile;
				 ?>
			</div>
		</div>
	</div>
</section>