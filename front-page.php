<?php get_header(); ?>

<section class="banner">
	<section class="banner-holder">
		<?php get_banner(); ?>
	</section>
</section>
<!-- FIM Menu e Banner -->

<section class="section presentaion-section">
	<div class="centered-container row">
		<div class="col-xs-12 col-sm-8 col-md-8">
			<div class="quick-links reveal-up">
				<div>
					<a href="<?php echo get_page_link(247); ?>">
						<h5>Equipe</h5>
						<p>Conheça os integrantes</p>
					</a>
				</div>
				<div>
					<a href="<?php echo get_category_link(2); ?>">
						<h5>Dicas de Saúde</h5>
						<p>Acesse nossas dicas</p>
					</a>
				</div>
				<div>
					<a href="#localmap">
						<h5>Localização</h5>
						<p>Como chegar</p>
					</a>
				</div>
				<div>
					<a href="<?php echo get_page_link(257); ?>">
						<h5>Fale Conosco</h5>
						<p>Dúvidas, informações, etc.</p>
					</a>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 relative">
			<?php get_template_part('part-procedimentos'); ?>
		</div>
	</div>
</section>

<section class="section about-section text-center">
	<div class="row">
		<div class="col-xs-12">
			<img class="reveal-up" src="<?php echo get_template_directory_uri(); ?>/img/dr-jefferson.jpg" alt="Dr. Jefferson Medeiros">
			<h4 class="reveal-up">Dr. Jefferson Moreira de Medeiros</h4>
			<p class="reveal-up">CRM-AM 4626</p>
			<hr>
			<p class="reveal-up">Membro Titular da Sociedade Brasileira de Cirurgia de Cabeça e Pescoço. <br>
			Professor de Clínica Cirúrgica da Universidade do Estado do Amazonas.</p>
			<a class="btn btn-light reveal-up" href="<?php echo get_page_link(2); ?>">sobre o médico</a>
		</div>
	</div>
</section>


<?php get_template_part('part-agendamento'); ?>



<?php get_footer(); ?>
