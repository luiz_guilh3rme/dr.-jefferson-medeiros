<ul class="social-share">
	<li>
		<a href="whatsapp://send?text=<?php echo get_permalink(); ?>" 
		   data-action="share/whatsapp/share" target="_blank">
			<i class="fa fa-fw fa-lg fa-whatsapp"></i>   	
		</a>
	</li>
	<li>
		<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank">
			<i class="fa fa-fw fa-lg fa-facebook"></i>
		</a>
	</li>
	<li>
		<a href="http://twitter.com/intent/tweet?text=<?php echo get_permalink(); ?>" target="_blank">
			<i class="fa fa-fw fa-lg fa-twitter"></i>
		</a>
	</li>
</ul>