<?php 
/*
	Template Name: Procedimentos
*/

get_header(); 
?>


<section class="page-content page-<?php echo $post->post_name ?>">

	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>


	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span>
						<?php the_title(); ?></span>
				</h1>
			</div>
		</div>

		<div class="row ">
			<div class="col-xs-12 col-sm-7 col-md-7 first-xs">
				<?php while(have_posts()) : the_post(); ?>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<?php endwhile; ?>
				<?php get_template_part('part-share'); ?>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 default-form">
				<?php  get_template_part('part-sideform'); ?>
			</div>
		</div>

	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>