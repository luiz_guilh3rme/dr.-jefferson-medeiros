<?php 

class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='sub-menu-wrap'><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

function get_banner(){
    global $banners;
    $banner = array();
    $args = array('post_type'=>'banner','posts_per_page'=>-1);
    
    $banners = new Wp_Query($args);

    while($banners->have_posts()){
        $banners->the_post();
        $banner_image = get_field('banner');
        $banner_responsive = get_field('banner_responsivo');
        $banner_link_post = get_field('link_post');
        $banner_link_categoria = get_field('link_categoria');
        $banner_link_personalizado = get_field('link_personalizado');
        $banner_legenda = get_field('legenda');
        $banner_sublegenda = get_field('sublegenda');
        $banner_legenda_pos = get_field('posicao_legenda');
        $banner_legenda_style = get_field('estilo_legenda');
        
        $banner_link = NULL;

        if($banner_link_post){
            $banner_link = $banner_link_post;
        }
        elseif($banner_link_categoria){
            $banner_link = get_category_link($banner_link_categoria);
        }
        elseif($banner_link_personalizado){
            $banner_link = $banner_link_personalizado;
        }

        if($banner_legenda){
            if(!empty($banner_link)){
                echo "<div>
                        <a href='" . $banner_link . "'>
                            <div class='banner-item'>
                                <div class='banner-item-inner'>
                                
                                    <img src='".$banner_image['url']."' alt='".get_the_title()."'>
                                    
                                    <div class='legenda ".$banner_legenda_pos ."'>
                                        <div class='legenda-inner'>
                                            <h1 class=".$banner_legenda_style.">".$banner_legenda."</h1>
                                            <h4>". $banner_sublegenda ."</h4>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>"; 
            }else{
                echo "<div>
                        <div class='banner-item'>
                            <div class='banner-item-inner'>
                               <img src='".$banner_image['url']."' alt='".get_the_title()."'>
                                <div class='legenda ".$banner_legenda_pos."'>
                                    <div class='legenda-inner'>
                                        <h1 class=".$banner_legenda_style.">".$banner_legenda."</h1>
                                        <h4>". $banner_sublegenda ."</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>";
            }
        }else{
            if(!empty($banner_link)){
                echo "<div>
                        <a href='" . $banner_link . "'>
                            <div class='banner-item'>
                                <div class='banner-item-inner'>
                                    <img src='".$banner_image['url']."' alt='".get_the_title()."'>     
                                </div>
                            </div>
                        </a>
                    </div>"; 
            }else{
                echo "
                    <div class='banner-item'>
                        <div class='banner-item-inner'>
                           <img src='".$banner_image['url']."' alt='".get_the_title()."'>
                        </div>

                    </div>";
            }

        }
    }
    wp_reset_postdata();
}

function custom_breadcrumbs() {
       
    // Settings
    $separator          = '/'; //&gt;
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Início';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {

            $cat = get_queried_object();

            $cat_parents = get_ancestors($cat->term_id,'category');

            foreach($cat_parents as $parents) {
                echo '<li class="item-cat"><a href="'. get_category_link($parents) .'">'.get_cat_name($parents).'</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
            }

               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Resultados para: ' . get_search_query() . '">Resultados para: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}


function page_banner(){

    $thumb = get_template_directory_uri() .'/img/page-banner.jpg';
    $title = get_the_title();

    echo "<section class=\"page-banner\">
              <img src=\"{$thumb}\" alt='{$title}'>
         </section>";
}


// SHOTCODE DE SERVIÇOS
function projects_shortcode(){
    ob_start(); ?>

<div class="projects-wrap projects-nav <?php if(is_single()){ echo 'projects-sigle'; } ?>">
<?php 
global $post;
$current = $post->ID;
// var_dump($current);
$projects = new WP_Query(array('post_type'=>'projeto','posts_per_page'=>6));
while($projects->have_posts()) : $projects->the_post(); ?>
     <div class="">
        <a class="project-link <?php if($current == get_the_id() and is_single()){ echo "project-link-current"; } ?>" href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
        </a>
     </div>
<?php endwhile; 
wp_reset_postdata(); ?>
</div>
<?php
    return ob_get_clean();
}
add_shortcode('projetos', 'projects_shortcode');


// ADICIONA CATEGORIA PADRÃO PARA CURSOS
function save_curso_meta( $post_id, $post, $update ) {

    $slug = 'curso'; //Slug of CPT

    // If this isn't a 'curso' post, don't update it.
    if ( $slug != $post->post_type ) {
        return;
    }
    $category = get_the_terms(get_the_id(),'categoria_curso');

    if(empty($category)){
        wp_set_object_terms( get_the_ID(), 10, 'categoria_curso' );
    }
}

add_action( 'save_post', 'save_curso_meta', 10, 3 );


// ADICIONA CATEGORIA PADRÃO PARA SERVIÇOS
function save_projeto_meta( $post_id, $post, $update ) {

    $slug = 'projeto'; //Slug of CPT

    // If this isn't a 'curso' post, don't update it.
    if ( $slug != $post->post_type ) {
        return;
    }
    
    $category = get_the_terms(get_the_id(),'categoria_servico');

    if(empty($category)){
        wp_set_object_terms( get_the_ID(), 9, 'categoria_servico' );
    }
}

add_action( 'save_post', 'save_projeto_meta', 10, 3 );

//FILTRA CURSOS PELA CATEGORIA
function filter_course_by_cat(){
    $cat_id = $_POST['cat_id'];

    if($cat_id == 0){
      $courses = new WP_Query(array('post_type'=>'curso'));
    }else{
        $courses = new WP_Query(array(
            'post_type'=>'curso',
            'tax_query' => array(
                array(
                    'taxonomy'         => 'categoria_curso',
                    'field'            => 'id',
                    'terms'            => $cat_id
                )
            )
        ));
    }

    ob_start();

    while($courses->have_posts()) : $courses->the_post(); 
    
        get_template_part('loop-course' ); 

    endwhile;

    ob_end_flush();

    echo $buffer;

    die();
}
add_action('wp_ajax_nopriv_filter_course_by_cat','filter_course_by_cat');
add_action('wp_ajax_filter_course_by_cat','filter_course_by_cat');

//FILTRA CURSOS PELO MES
function filter_course_by_month(){
    $month = $_POST['month'];

    if($month == 0){
      $courses = new WP_Query(array('post_type'=>'curso'));
    }else{
        $first_date = date('2017'.$month.'01');
        $last_date = date('2017'.$month.'t');

         $args = array (
            'post_type' => 'curso',
            'orderby'   => 'meta_value_num',
            'meta_query' => array(
                array(
                    'key'       => 'data_inicio',
                    'compare'   => 'BETWEEN',
                    'value'     => array($first_date, $last_date)
                )
            )
        );

       $courses = new WP_Query($args);
    }

    while($courses->have_posts()) : $courses->the_post(); 
        get_template_part('loop-course');
    endwhile;


    die();
}
add_action('wp_ajax_nopriv_filter_course_by_month','filter_course_by_month');
add_action('wp_ajax_filter_course_by_month','filter_course_by_month');


function new_contact_methods( $contactmethods ) {
    $contactmethods['phone'] = 'Telefone';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );


function new_modify_user_table( $column ) {
    $column['phone'] = 'Telefone';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'phone' :
            return get_the_author_meta( 'phone', $user_id );
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );


/**
 * Registering a new user.
 */
add_action('template_redirect', 'register_user');
 
function register_user(){
  if(isset($_GET['do']) && $_GET['do'] == 'register'):
    $errors = array();
    if(empty($_POST['user'])) 
       $errors[] = 'Por favor digite seu nome completo.<br>';
    if(empty($_POST['email'])) 
       $errors[] = 'Digite seu email.<br>';
    if(empty($_POST['pass'])) 
       $errors[] = 'Digite uma senha.<br>';
    if(empty($_POST['cpass'])) 
       $errors[] = 'Confirmar senha.<br>';
    if((!empty($_POST['cpass']) && !empty($_POST['pass'])) && ($_POST['pass'] != $_POST['cpass'])) 
       $errors[] = 'As senhas não correspondem.';
        $user_login = esc_attr($_POST['user']);
        $user_email = esc_attr($_POST['email']);
        $user_pass = esc_attr($_POST['pass']);
        $user_confirm_pass = esc_attr($_POST['cpass']);
        $user_phone = esc_attr($_POST['phone']);
        $sanitized_user_login = sanitize_user($user_login);
        $user_email = apply_filters('user_registration_email', $user_email);
  
    if(!is_email($user_email)) 
       $errors[] = 'Email inválido.<br>';
    elseif(email_exists($user_email)) 
       $errors[] = 'Este email já esta cadastrado.<br>';
  
    if(empty($sanitized_user_login) || !validate_username($user_login)) 
       $errors[] = 'Nome de usuário inválido.<br>';
    elseif(username_exists($sanitized_user_login)) 
       $errors[] = 'Este nome de usuário já existe.<br>';
  
    if(empty($errors)):
      $user_id = wp_create_user($sanitized_user_login, $user_pass, $user_email);
  
    if(!$user_id):
      $errors[] = 'Não foi possível efetuar o cadastro.';
    else:
      update_user_option($user_id, 'default_password_nag', true, true);
      wp_new_user_notification($user_id, $user_pass);
      update_user_meta ($user_id, 'phone', $user_phone);
      wp_cache_delete ($user_id, 'users');
      wp_cache_delete ($user_login, 'userlogins');
      do_action ('user_register', $user_id);
      $user_data = get_userdata ($user_id);
      if ($user_data !== false) {
         // wp_clear_auth_cookie();
         // wp_set_auth_cookie ($user_data->ID, true);
         // do_action ('wp_login', $user_data->user_login, $user_data);
         // Redirect user.
        $msg = "Cadastro+realizado+com+sucesso!";
         // wp_redirect ('?page_id=24&msg='.$msg);
         wp_redirect ( get_page_link(24) . '/?msg='.$msg);
         // wp_redirect (add_query_arg( 'msg', $msg, get_page_link(24)));
         exit();
       }
      endif;
    endif;
  
    if(!empty($errors)) 
      define('REGISTRATION_ERROR', serialize($errors));
  endif;
}
