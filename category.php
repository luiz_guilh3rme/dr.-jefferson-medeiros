<?php get_header(); 
$cat = get_queried_object(); ?>


<section class="page-content page-<?php echo $post->post_name ?>">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span><?php $cat->name ?></span>
				</h1>
			</div>
			<div class="col-xs-12 no-padding">
				<div class="row">
					<?php 
						while(have_posts()) : the_post();
					 ?>
					 <div class="col-xs-12 col-sm-4">
					 	<div class="news-item">
							<a href="<?php the_permalink(); ?>">
								<figure>
									<?php the_post_thumbnail(); ?>
								</figure>
								<p><?php the_title(); ?></p>
							</a>
							<div class="text-center">
								<?php get_template_part('part-share'); ?>
							</div>
						</div>
					</div>
					 <?php 
					 	endwhile;
					  ?>
				</div>
			</div>
		</div>
		<div class="pagination">
			<?php
				global $wp_query;

				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages,
					'prev_text' => __('«'),
					'next_text' => __('»')
				) );
			?>
		</div>
	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>

