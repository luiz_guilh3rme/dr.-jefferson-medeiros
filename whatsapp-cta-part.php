<div class="whatsapp-wrapper">
    <button class="close-whatsapp tracked" aria-label="esconder clique para falar - whatsapp">
        ×
    </button>
    <a href="wpp" rel="nofollow noopener" target="_BLANK" data-category="Whatsapp" title="Fale conosco no Whatsapp"
        target="_BLANK" class="wpp-lateral">
        <img src="<?php bloginfo('template_url'); ?>/img/zap.png" alt="Logotipo whatsapp em verde">
    </a>
</div>
<div class="mobile-whatsapp-wrapper">
    <button class="mobile-whatsapp-btn te-ligamos-mobile">
        NÓS TE LIGAMOS
    </button>
    <a href="https://wa.me/5592984206381" target="_BLANK" rel="nofollow noopener" class="mobile-whatsapp-btn wpp-mobile">
        NOSSO WHATSAPP
    </a>
</div>

<script>
 $(".close-whatsapp").on("click", function() {
        $(".whatsapp-wrapper").addClass("clicked")
    })
</script>