<?php get_header(); ?>


<section class="page-content">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">
		
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green-title small-title">
					<span>Erro 404</span>
				</h1>
			</div>
		</div>

		<div class="entry-content cf">

			<h2>Página não encontrada!</h2>
			<p>Utilize o campo de pesquisa para tentar encontrar o que você procura.</p>
			
		</div>

	</div>

</section>

<?php get_template_part('part-noticias'); ?>

<?php get_template_part('part-faca-parte'); ?>

<?php get_footer(); ?>


