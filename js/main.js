function initMap() {
	var e = [{
			stylers: [{
				hue: "#4a7fa7"
			}, {
				saturation: -20
			}]
		}, {
			featureType: "road",
			elementType: "geometry",
			stylers: [{
				lightness: 100
			}, {
				visibility: "simplified"
			}]
		}, {
			featureType: "road",
			elementType: "labels",
			stylers: [{
				visibility: "on"
			}]
		}],
		a = new google.maps.StyledMapType(e, {
			name: "Oncoclin"
		}),
		o = new google.maps.Map(document.getElementById("localmap"), {
			center: {
				lat: -3.1208602,
				lng: -60.0075458
			},
			scrollwheel: !1,
			zoom: 19,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, "map_style"]
			}
		});
	o.mapTypes.set("map_style", a), o.setMapTypeId("map_style");
	var n = new google.maps.Marker({
			map: o,
			position: {
				lat: -3.1208602,
				lng: -60.0075458
			},
			title: "Oncoclin"
		}),
		s = new google.maps.InfoWindow({
			content: "<h2 style='color:#595959'>Oncoclin</h2><p>Av. Castelo Branco, 1779 - Cachoeirinha</p>"
		});
	n.addListener("click", function () {
		s.open(o, n)
	})
}
$(document).ready(function (e) {
	e(".menu-button").click(function (a) {
		e(this).toggleClass("menu-rotate"), e(this).find("div").toggleClass("menu-button-active"), e("nav").toggleClass("showMenu"), e(".search-form-wrap").removeClass("search-opened")
	});
	var a = e(".banner-holder");
	if (a.slick({
			dots: !0,
			autoplay: !0,
			autoplaySpeed: 5e3,
			speed: 600,
			loop: !0,
			centerMode: !1,
			arrows: !0,
			prevArrow: '<i class="fa fa-2x fa-angle-left banner-nav banner-nav-prev"></i>',
			nextArrow: '<i class="fa fa-2x fa-angle-right banner-nav banner-nav-next"></i>',
			pauseOnHover: !1
		}), e(".testimonials-carousel").slick({
			slidesToShow: 3,
			arrows: !1,
			autoplay: !0,
			responsive: [{
				breakpoint: 801,
				settings: {
					slidesToShow: 2,
					centerMode: !1
				}
			}, {
				breakpoint: 641,
				settings: {
					slidesToShow: 1,
					centerMode: !1
				}
			}]
		}), e(".showProc").click(function (a) {
			e(".procedures-list").toggleClass("full-list")
		}), e(window).scroll(function (a) {
			e(this).scrollTop() > 700 ? e(".backToTop").fadeIn() : e(".backToTop").fadeOut()
		}), e(".backToTop").click(function (a) {
			e("html, body").animate({
				scrollTop: 0
			}, 1e3)
		}), e(".search-btn .fa").click(function (a) {
			e(".search-form-wrap").slideToggle("fast"), a.stopPropagation()
		}), e(document.body).click(function (a) {
			e(".search-form-wrap").slideUp("fast")
		}), e(".search-form-wrap").click(function (e) {
			e.stopPropagation()
		}), e(".close-search").click(function (a) {
			a.preventDefault(), e(".search-form-wrap").removeClass("search-opened")
		}), e(".open-chat").click(function (a) {
			a.preventDefault(), e("#sc_chat_box").css("bottom", "0px").addClass("sc-chat-css-anim")
		}), e(document).on("keypress", ".search-opened", function (a) {
			27 == a.keyCode && e(this).removeClass("search-opened")
		}), window.sr = ScrollReveal({
			reset: !1
		}), sr.reveal(".reveal-up", {
			duration: 1500,
			scale: 1,
			distance: "200px"
		}), sr.reveal(".reveal-down", {
			origin: "top",
			duration: 800,
			scale: 1,
			distance: "100px"
		}), sr.reveal(".reveal-left", {
			origin: "left",
			duration: 1e3,
			scale: 1,
			distance: "200px"
		}), e("form input,form textarea").focus(function (a) {
			e(this).next().hide(), e(".wpcf7-response-output").slideUp()
		}), e(".children-swicther a").first().addClass("ativo"), e(".children-swicther a").click(function (a) {
			e(".children-swicther a").removeClass("ativo"), this.setAttribute("class", "ativo");
			var o = this.getAttribute("data-child");
			e(".children-content").hide(), e(".children-" + o).show()
		}), screen.width <= 1024) {
		e(".menu > li > a").click(function (a) {
			e(this).parent().find(".sub-menu-wrap").length && (a.preventDefault(), e("div", e(this).parent()).slideToggle())
		})
	}
	screen.width <= 800, screen.width < 641 && (e(".gallery-wrap .gallery").find("br").remove(), e(".gallery-wrap .gallery-item").css("width", "50%"))
}), $(window).load(function () {
	function e() {
		$(".bg-modal").addClass("show-bg-modal"), $(".warning-wrapper").addClass("show-warning")
	}
	$(".close-warning").click(function (e) {
		$(".bg-modal").removeClass("show-bg-modal"), $(".warning-wrapper").removeClass("show-warning")
	}), $(".close-warning,.bg-modal").click(function (e) {
		$(".bg-modal").removeClass("show-bg-modal"), $(".warning-wrapper").removeClass("show-warning")
	}), $(this).keypress(function (e) {
		27 == e.keyCode && ($(".bg-modal").removeClass("show-bg-modal"), $(".warning-wrapper").removeClass("show-warning"))
	}), $(".warning-wrapper").data("enable") && setTimeout(e, 2e3)
});



$(document).ready(function () {
	function openPhoneCta() {
		// duhh
		$('.open-cta, .confirm, .te-ligamos-mobile').on('click', function () {
			$('.overlay').fadeIn(400, function () {
				$('.form-wrapper-all').fadeIn();
			});


			// create new date on button click
			var now = new Date(),
				days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
				day = days[now.getDay()],
				hour = now.getHours(),
				picker = $('.form-pickers[data-instance="00"]');


			// check if it is outside business hours
			if (day === 'Domingo' || day === 'Sábado' || hour > 18 || hour <= 9) {
				picker[0].disabled = true;
				picker[0].title = "Disponível apenas em horário comercial";
			}
		});
	}

	function formPickers() {
		$('.form-pickers').removeClass('active');
		$(this).addClass('active');
		var instance = $(this).data('instance');

		$('.instance:not([data-instance=' + instance + '])').fadeOut(400, function () {
			setTimeout(function () {
				$('.instance[data-instance=' + instance + ']').fadeIn()
			}, 400)
		});
	}

	function closePhoneCta() {
		$('.close-modal').on('click', function () {
			$('.form-wrapper-all').fadeOut(400, function () {
				$('.overlay').fadeOut();
			});
		});
	}

	openPhoneCta();
	closePhoneCta();
	$('.form-pickers').on('click', formPickers);

	function randomizeRequests(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}

	function printNumbers() {
		for (var e = document.querySelectorAll(".number"), t = 0; t < e.length; t++)
			e[t].innerHTML = randomizeRequests(1, 12)
	}

	printNumbers();

	$(".close-overlay").on("click", function () {
		$(".form-wrapper-all, .informative").fadeOut(400, function () {
			$(".overlay").fadeOut()
		})
	})

	$('.close-phone-cta').on('click', function () {
		$('.call-cta-wrapper').addClass('clicked');
	});


	function setCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "") + expires + "; path=/";
	}


	function getCookie(name) {
		var dc = document.cookie;
		var prefix = name + '=';
		var begin = dc.indexOf('; ' + prefix);
		if (begin == -1) {
			begin = dc.indexOf(prefix);
			if (begin !== 0) return null;
		} else {
			begin += 2;
			var end = document.cookie.indexOf(';', begin);
			if (end == -1) {
				end = dc.length;
			}
		}

		return decodeURI(dc.substring(begin + prefix.length, end));
	}

	function doCookieStuff() {
		var cookie = getCookie('modal');

		if (cookie === null) {
			setCookie('modal', 'visitou no último dia', 1);

			$('.overlay').fadeIn(400, function () {
				$('.informative').fadeIn();
			});

		}
	}

	// doCookieStuff();

});