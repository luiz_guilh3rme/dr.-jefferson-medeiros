<?php get_header(); 
$cat = get_the_category(); ?>


<section class="page-content page-<?php echo $post->post_name ?>">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span><?php the_title(); ?></span>
				</h1>
			</div>
			<div class="col-xs-12 no-padding">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8">
						<?php 
							if(has_post_thumbnail()):
						 ?>
						<div class="post-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php 
							endif;
						 ?>
						<div class="entry-content">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<h4 style="color: #4b616c; padding-left: 20px;margin-bottom: 40px;">Veja também</h4>
						<?php 
							$related = new WP_Query(array('cat'=>$cat[0]->term_id,'posts_per_page'=>3,'post__not_in'=>array(get_the_id()))); 
							while($related->have_posts()) : $related->the_post();
						?>
						<div class="news-item">
							<a href="<?php the_permalink(); ?>">
								<figure>
									<?php the_post_thumbnail(); ?>
								</figure>
								<p><?php the_title(); ?></p>
							</a>
						</div>
						<?php 
							endwhile;
						 ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>

