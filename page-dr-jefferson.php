<?php get_header(); ?>

<section class="page-content">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span>Sobre o Médico</span>
				</h1>
			</div>
		</div>

		<div class="row">
		<?php 
			while(have_posts()) : the_post();
		 ?>
			<div class="col-xs-12 col-sm-5 col-md-5">
				<figure class="doctor-image">
					<?php the_post_thumbnail(); ?>
				</figure>
				<?php get_template_part('part-procedimentos'); ?>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<?php get_template_part('part-share'); ?>
			</div>
		<?php 
			endwhile;
		 ?>

		</div>

	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>

