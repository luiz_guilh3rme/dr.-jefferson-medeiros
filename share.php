<div class="share">
	<ul class="share-buttons">
		<li>
			<a class="share-facebook" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" title="Compartilhe no Facebook" target="_blank"></a>
		</li>
		<li>
			<a class="share-twitter" href="http://twitter.com/intent/tweet?text=<?php echo get_permalink(); ?>" title="Compartilhe no Twitter" target="_blank"></a>
		</li>
		<li>
			<a class="share-pinterest" 
			   href="http://pinterest.com/pin/create/button/?url=<?php echo get_permalink(); ?>" 
			   title="Compartilhe no Pinterest" target="_blank"></a>
		</li>
		<li>
			<a class="share-email" href="mailto:" title="Compartilhe pelo seu Email" target="_blank"></a>
		</li>
		<li>
			<a class="share-whatsapp" href="whatsapp://send?text=<?php echo get_permalink() ?>" title="Compartilhe no Whatsapp" target="_blank"></a>
		</li>
	</ul>
</div>