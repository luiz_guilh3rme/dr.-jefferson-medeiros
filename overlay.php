<div class="overlay">
    <button class="close-overlay" aria-label="Fechar Modal">×</button>
    <!-- <div class="informative">
        <div class="text-wrapper">
            <p class="informative-text">
                Queremos agradecer a todos os nossos pacientes que estiveram conosco durante todo o ano.
            </p>
            <p class="informative-text extra-spaced">
                Informamos que entraremos em recesso a partir do dia <b>19/12/2018</b> até o dia <b>02/01/2019</b>,
				retomando nosso atendimento no dia <b>03/01/2019</b>.</p>
			
			<p class="informative-text">Para agendamentos, estamos disponíveis pelo número: <b>(92) 3307.4968</b>.</p>
            <p class="informative-text slightly-bigger">
                <b>
                    Desejamos à todos um Próspero Natal e um Feliz Ano Novo de muito sucesso.
                </b>
            </p>
            <p class="informative-name">
                Atenciosamente, <br>
                Dr. <b> Jefferson Medeiros</b>
            </p>
        </div>
        <img src="<?php // bloginfo('template_url'); ?>/img/cups.png" alt="Copos de Champanhe brindando com fita decorativa" title="Boas Festas" class="cups">
        <img src="<?php // bloginfo('template_url'); ?>/img/jeffersonpersona.png" alt="Dr. Jefferson sorrindo e vestindo terno" title="Dr. Jefferson" class="jeff-persona">
    </div> -->
    <div class="form-wrapper-all">
        <div class="form-picker">
            <button class="form-pickers" data-instance="00">
                <i class="fa fa-phone"></i>
                ME LIGUE AGORA
            </button>
            <button class="form-pickers active" data-instance="01">
                <i class="fa fa-calendar"></i>
                ME LIGUE DEPOIS
            </button>
            <button class="form-pickers" data-instance="02">
                <i class="fa fa-comments alt"></i>
                DEIXE UMA MENSAGEM
            </button>
        </div>
        <div class="instance second" data-instance="01">
            <div class="leave-message schedule-time" data-form="schedule">
                <legend class="leave-title">
                    Gostaria de agendar e receber uma
                    chamada em outro horário?
                </legend>
                <div class="fields cleared">
                    <?php echo do_shortcode('[contact-form-7 id="618" title="Ligar Depois"]'); ?>
                    <p class="callers">Você já é a <span class="number">10</span> pessoa a solicitar uma ligação.</p>
                </div>
            </div>
        </div>
        <div class="instance" data-instance="02">
            <div class="leave-message" data-form="leave-message">
                <legend class="leave-title">
                    Deixe sua mensagem! Entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="fields cleared">
                    <?php echo do_shortcode('[contact-form-7 id="619" title="Deixe sua Mensagem"]'); ?>
                    <p class="callers">Você já é a <span class="number">10</span> pessoa a deixar uma mensagem.</p>
                </div>
            </div>
        </div>
        <div class="instance active" data-instance="00">
            <div class="leave-message " data-form="we-call">
                <legend class="leave-title">
                    <span class="variant">
                        NÓS TE LIGAMOS!
                    </span>
                    Informe seu telefone que entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="fields cleared">
                    <?php echo do_shortcode('[contact-form-7 id="620" title="Me ligue Agora"]'); ?>
                    <p class="callers">Você já é a <span class="number">1</span> pessoa a solicitar uma ligação.</p>
                </div>
            </div>
        </div>
    </div>
</div>