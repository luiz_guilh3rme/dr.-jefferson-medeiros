<?php get_header(); ?>


<section class="page-content page-<?php echo $post->post_name ?>">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span><?php the_title(); ?></span>
				</h1>
			</div>
			<div class="col-xs-12">
				<div class="entry-content cf">
				<?php  
					$children = get_pages(array('parent'=>$post->ID));
					if(!empty($children)):
						foreach($children as $post): setup_postdata( $post );
				?>
					
					<a class="search-result" href="<?php the_permalink(); ?>">
						 <h3><?php the_title(); ?></h3>
						 <p><?php echo get_the_excerpt(); ?></p>
					</a>	

				<?php
						endforeach;
					else:
						while(have_posts()) : the_post(); 

							the_content();

						endwhile; 
					endif;
				?>
				</div>
			</div>
		</div>
		<?php get_template_part('part-share'); ?>
	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>

