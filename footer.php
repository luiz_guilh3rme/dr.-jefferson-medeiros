<?php global $mydata; ?>

<section class="map">
	<div id="localmap"></div>
</section>
<footer>
	<div class="centered-container row">
		<div class="col-xs-12">
			<div class="footer-info-wrap">
				<div class="footer-logo reveal-up">
					<a href="<?php echo esc_url(home_url()); ?>/">
						<img src="<?php echo $mydata->footer_logo ?>" alt="RoraimaNET" title="RoraimaNET">
					</a>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="info-box reveal-up">
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icn-marker.png">
					Av. Jornalista Umberto Calderaro, 455 - sl.915 - Adrianápolis - Manaus/AM
				</p>
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icn-marker.png">
					<?php echo $mydata->address ?>
				</p>
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icn-phone.png">
					<?php echo $mydata->phones ?>
				</p>
				<p>
					<?php echo $mydata->city . ' - ' . $mydata->state ?>
				</p>
			</div>
		</div>
		<div class="col-xs-12 col-md-3">
			<div class="info-box reveal-up">
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icn-clock.png">
					Horário de Funcionamento:
				</p>
				<p>
					<?php echo $mydata->schedule_business ?>
				</p>
				<p>
					<?php echo $mydata->schedule_weekend ?>
				</p>
			</div>
		</div>
		<div class="col-xs-12 col-md-5">
			<div class="footer-midia reveal-up">
				<a class="instagram-link" target="_blank" href="<?php echo $mydata->instagram ?>">Siga no Instagram <i class="fa fa-2x fa-instagram"></i></a>
				<div class="facebook-box">
					<div class="fb-page" data-href="https://www.facebook.com/jeffersonmedeiros.com.br/?ref=ts&amp;fref=ts" data-width="500"
					 data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
						<blockquote cite="https://www.facebook.com/jeffersonmedeiros.com.br/?ref=ts&amp;fref=ts" class="fb-xfbml-parse-ignore"><a
							 href="https://www.facebook.com/jeffersonmedeiros.com.br/?ref=ts&amp;fref=ts">Dr. Jefferson Medeiros - Cirurgião
								de Cabeça e Pescoço</a></blockquote>
					</div>
				</div>
				<?php echo $mydata->get_social(true, 'footer-social-list'); ?>
			</div>
		</div>

	</div>
</footer>
<div class="credit">
	<div class="centered-container">
		<p>Desenvolvido por:</p>
		<a href="http://somavirtual.com.br">
			<img src="<?php echo get_template_directory_uri(); ?>/img/logo-soma-virtual.png" alt="Soma Virtual" title="Soma Virtual">
		</a>
	</div>
</div>


<div class="backToTop">
	<i class="fa fa-angle-up"></i>
</div>

</div>
<div id="fb-root"></div>
<script>
	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>


<?php wp_footer(); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $mydata->get_api_key() ?>&callback=initMap" async
 defer></script>
<?php 
get_template_part('whatsapp-cta-part');
get_template_part('call-cta');
get_template_part('overlay');
 ?>
</body>

</html>