<?php get_header(); ?>


<section class="page-content page-<?php echo $post->post_name ?>">
	
	<div class="centered-container row">
		<div class="col-xs-12">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	
	<div class="centered-container">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<span><?php the_title(); ?></span>
				</h1>
			</div>
			<div class="col-xs-12">
				
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 no-padding">
						<?php while(have_posts()) : the_post(); ?>
						<div class="entry-content cf">
							<?php the_content(); ?>	
						</div>
						<?php endwhile; ?>

						<div class="default-form">
							<?php echo do_shortcode('[contact-form-7 id="325" title="Contato"]'); ?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 no-padding">
						<div class="doctor-caricature">
							<img src="<?php echo get_template_directory_uri(); ?>/img/dr-jefferson-caricatura.jpg">
							<p>Dr. Jefferson Moreira de Medeiros</p>
							<p>CRM-AM 4626</p>
						</div>
						<?php get_template_part('part-share'); ?>
					</div>
				</div>

				<div class="contact-info">
				<?php global $mydata; ?>
					<p>Oncoclin de Manaus | <?php echo $mydata->address .' - ' . $mydata->city .'/' . $mydata->state ?></p>
					<p><?php echo $mydata->phones; ?> - <i class="fa fa-lg fa-whatsapp"></i> <?php echo $mydata->whatsapp ?></p>
				</div>
			</div>
		</div>
	</div>

</section>

<?php get_template_part('part-agendamento'); ?>

<?php get_footer(); ?>

