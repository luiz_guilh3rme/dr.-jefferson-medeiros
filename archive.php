<?php get_header(); ?>
	
<?php page_banner() ?>

<section class="content centered-container row">

	<div class="col-md-12">
		<?php custom_breadcrumbs(); ?>
	</div>
	
	<div class="col-md-12">
		
		<h1 class="page-title">
			<span class="title-detail">
				<?php the_title(); ?>
			</span>
		</h1>

		<div class="content">
			<section class="entry-content">

			<?php while(have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>

			</section>
		</div>
	</div>
</section>

<div class="line"></div>

<?php get_template_part('part-cursos'); ?>

<div class="line"></div>

<?php get_template_part('part-clientes'); ?>

<?php get_footer(); ?>


