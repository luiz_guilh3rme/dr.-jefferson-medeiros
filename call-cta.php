<div class="call-cta-wrapper">
    <div class="tooltip">
        <p class="tooltip-text">
            <span class="variant">Olá!</span>
            Gostaria de receber uma ligação gratuita?</p>
        <button class="confirm tracked" data-category="CTA - Telefone" data-action="Click" data-label="Abriu CTA de Telefone">
            SIM
        </button>
        <button class="close-phone-cta" aria-label="Fechar CTA de telefone">
            &times; 
        </button>
    </div>
    <img src="<?php bloginfo('template_url'); ?>/img/callcta.png" aria-hidden="true" class="call-cta">
</div>